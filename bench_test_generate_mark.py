#!/usr/bin/env -S python3
import sys
import math

def error():
    if len(sys.argv) == 1:
        sys.exit(84)
    elif sys.argv[0] == "./103cipher" and sys.argv[1] == "-h" and len(sys.argv) == 2:
        help()
        exit(0)
    elif sys.argv[0] == "./103cipher" and sys.argv[1] == "-h" and len(sys.argv) > 2:
        sys.exit(84)
    elif (len(sys.argv) != 4):
        sys.exit(84)
    elif (len(sys.argv[2]) not in range (1, 15)):
        sys.exit(84)
    return 0

def help():
    my_str = """USAGE
    ./103cipher message key flag

DESCRIPTION
    message     a message, made of ASCII characters
    key         the encryption key, made of ASCII characters
    flag        0 for the message to be encrypted, 1 to be decrypted
    """
    print(my_str)
    return 0

def switch():
    flag = int(sys.argv[3])
    if (flag == 0):
        key()
        func_print()
    elif (flag == 1):
        reverse()
    else :
        sys.exit(84)


def matrice_computing(matrice_0, matrice_1):
    x = int()
    matrice2 = [[0, 0, 0]]
    if (len(matrice_0) > len(matrice2)):
        for i in range(len(matrice_0) - 1):
            extension = [[0, 0, 0]]
            matrice2.extend(extension)
    for i in range(len(matrice_0)):
        for e in range(3):
            for p in range(3):
                x += matrice_0[i][p] * matrice_1[p][e]
            matrice2[i][e] = x
            x = 0
    return matrice2

def matrice_computing1_1(matrice_0, matrice_1):
    x = int()
    matrice2 = [[0]]
    if (len(matrice_0) > len(matrice2)):
        for i in range(len(matrice_0) - 1):
            extension = [[0]]
            matrice2.extend(extension)
    for i in range(len(matrice_0)):
        for e in range(1):
            for p in range(1):
                x += matrice_0[i][p] * matrice_1[p][e]
            matrice2[i][e] = x
            x = 0 
    return matrice2

def matrice_computing4_4(matrice_0, matrice_1):
    x = int()
    matrice2 = [[0, 0]]
    if (len(matrice_0) > len(matrice2)):
        for i in range(len(matrice_0) - 1):
            extension = [[0, 0]]
            matrice2.extend(extension)
    for i in range(len(matrice_0)):
        for e in range(2):
            for p in range(2):
                x += matrice_0[i][p] * matrice_1[p][e]
            matrice2[i][e] = x
            x = 0
    return matrice2


def sys_argv_1():
    list = sys.argv[1]
    array = [ord(i) for i in list]
    determinant = (math.ceil((len(array) / 3)) * 3) - (len(array))
    if determinant == 2:
        extension = [0, 0]
        array.extend(extension)
    elif determinant == 1:
        extension = [0]
        array.extend(extension)
    array = [array[i:i+3] for i in range(0, len(array), 3)]
    return array

def sys_argv_1_1_1():
    list = sys.argv[1]
    array = [ord(i) for i in list]
    array = [array[i:i+1] for i in range(0, len(array), 1)]
    return array

def sys_argv_1_4_4():
    list = sys.argv[1]
    array = [ord(i) for i in list]
    array = [array[i:i+2] for i in range(0, len(array), 2)]
    return array


def key_matrice():
    stock = sys.argv[2]
    if len(stock) < 10:
        determinant = 9 - len(stock)
        for i in range(determinant):
            stock += "\0"
    ascii_matrice = [ord(i) for i in stock]
    matrice_key = [[ascii_matrice[0], ascii_matrice[1], ascii_matrice[2]],
                    [ascii_matrice[3], ascii_matrice[4], ascii_matrice[5]],
                    [ascii_matrice[6], ascii_matrice[7], ascii_matrice[8]]]
    return matrice_key

def key_matrice_1_1():
    stock = sys.argv[2]
    if len(stock) < 10:
        determinant = 9 - len(stock)
        for i in range(determinant):
            stock += "\0"
    ascii_matrice = [ord(i) for i in stock]
    matrice_key = [[ascii_matrice[0]]]
    return matrice_key

def key_matrice_4_4():
    stock = sys.argv[2]
    if len(stock) < 10:
        determinant = 9 - len(stock)
        for i in range(determinant):
            stock += "\0"
    ascii_matrice = [ord(i) for i in stock]
    matrice_key = [[ascii_matrice[0], ascii_matrice[1]],
                    [ascii_matrice[2], ascii_matrice[3]]]
    return matrice_key


def key():
    if len(sys.argv[2]) == 1:
        matrice_0 = sys_argv_1_1_1()
        matrice_1 = key_matrice_1_1()
        array_result = matrice_computing1_1(matrice_0, matrice_1)
    elif len(sys.argv[2]) in range(1, 5):
        matrice_0 = sys_argv_1_4_4()
        matrice_1 = key_matrice_4_4()
        array_result = matrice_computing4_4(matrice_0, matrice_1)
    else :
        matrice_0 = sys_argv_1()
        matrice_1 = key_matrice()  
        array_result = matrice_computing(matrice_0, matrice_1)

    array_result = " ".join(str(x) for x in array_result)
    array_result = array_result.replace("[", "").replace("]", "").replace(",", "")
    return array_result

def func_print_2_2():
    print("Bench_result :")
    if len(sys.argv[2]) == 1:
        print("%d" % (key_matrice_1_1()[0][0]))
        print("\nBench_mark_core_test:")
        print(key())
    elif len(sys.argv[2]) in range(1, 5):
        for i in range(2):
            print("%d\t%d" % (key_matrice_4_4()[i][0], key_matrice_4_4()[i][1]))
        print("\nBench_mark_core_test:")
        print(key())

def func_print():
    if len(sys.argv[2]) in range(1,5):
        func_print_2_2()
    else :
        print("Bench_result :")
        for i in range(3):
            print("%d\t%d\t%d" % (key_matrice()[i][0], key_matrice()[i][1], key_matrice()[i][2]))
        print("\nBench_mark_core_test:")
        print(key())
    return 0


def reverse_computing_matrice(matrice_1):
    determinant = matrice_1[0][0] * (
            (matrice_1[1][1] * matrice_1[2][2]) - (matrice_1[1][2] * matrice_1[2][1])) - matrice_1[0][1] * (
                (matrice_1[1][0] * matrice_1[2][2]) - (matrice_1[1][2] * matrice_1[2][0])) + matrice_1[0][2] * (
                (matrice_1[1][0] * matrice_1[2][1]) - (matrice_1[1][1] * matrice_1[2][0]))
    co_facteur_1 = [(matrice_1[1][1] * matrice_1[2][2]) - (matrice_1[1][2] * matrice_1[2][1]),
                -((matrice_1[1][0] * matrice_1[2][2]) - (matrice_1[1][2] * matrice_1[2][0])),
                (matrice_1[1][0] * matrice_1[2][1]) - (matrice_1[1][1] * matrice_1[2][0])]

    co_facteur_2 = [-((matrice_1[0][1] * matrice_1[2][2]) - (matrice_1[0][2] * matrice_1[2][1])),
                (matrice_1[0][0] * matrice_1[2][2]) - (matrice_1[0][2] * matrice_1[2][0]),
                -((matrice_1[0][0] * matrice_1[2][1]) - (matrice_1[0][1] * matrice_1[2][0]))]

    co_facteur_3 = [(matrice_1[0][1] * matrice_1[1][2]) - (matrice_1[0][2] * matrice_1[1][1]),
                -((matrice_1[0][0] * matrice_1[1][2]) - (matrice_1[0][2] * matrice_1[1][0])),
                (matrice_1[0][0] * matrice_1[1][1]) - (matrice_1[0][1] * matrice_1[1][0])]

    matrice_inverse = [[1 / determinant * (co_facteur_1[0]), 1 / determinant * (co_facteur_2[0]), 1 / determinant * (co_facteur_3[0])],
                [1 / determinant * (co_facteur_1[1]), 1 / determinant * (co_facteur_2[1]), 1 / determinant * (co_facteur_3[1])],
                [1 / determinant * (co_facteur_1[2]), 1 / determinant * (co_facteur_2[2]), 1 / determinant * (co_facteur_3[2])]]

    return matrice_inverse

def encode():
    global nb
    i, e, z, a = 0, 0, 0, 1

    array = [[0, 0, 0]]
    list = sys.argv[1]
    
    for i in range(len(sys.argv[1])):
        if sys.argv[1][i] == ' ':
            a += 1
    for h in range(math.ceil(a / 3) - 1):
            extension = [[0, 0, 0]]
            array.extend(extension)
            
    list = list.split(" ")

    if ((len(list)) % 3) == 1:
        for i in range(2):
            list.append(0)
    elif ((len(list)) % 3) == 2:
        list.append(0)
    l = len(list)
    nb = (math.ceil(l)) / 3
    if float.is_integer(nb) == False:
        nb += 1
    nb = int(nb)

    for z in range(nb):
        for o in range(3):
            array[z][o] = int(list[e])
            e += 1
    return array

def reverse():
    if len(sys.argv[2]) == 1:
        return 0
    elif len(sys.argv[2]) in range(1, 5):
        return 0

    matrice_0 = encode()
    matrice_1 = key_matrice()

    matrice_1 = reverse_computing_matrice(matrice_1)
    matrice_2 = matrice_computing(matrice_0, matrice_1)

    for i in range(nb):
        for e in range(3):
            matrice_2[i][e] = round(matrice_2[i][e])

    for i in range(nb):
        for e in range(3):
            matrice_2[i][e] = chr(matrice_2[i][e])
            if matrice_2[i][e] == '\x00':
                matrice_2[i][e] = '_@_'

    string = ""
    matrice_2 = " ".join(str(x) for x in matrice_2)
    matrice_2 = matrice_2.replace(", ", "").replace("], ", "").replace("] [", "").replace("[", "").replace("''", "").replace("'", "").replace('""', "'").replace("_@_", "")
    matrice_2 = matrice_2[:-1]
    for x in matrice_2:
        string += x
        
    print("Bench_result :")
    for i in range(3):
        if (matrice_1[i][0] == -0.00):
            matrice_1[i][0] = 0.0
        if (matrice_1[i][1] == -0.00):
            matrice_1[i][1] = 0.0
        if (matrice_1[i][2] == -0.00):
            matrice_1[i][2] = 0.0
        print("%s\t%s\t%s" % (round(matrice_1[i][0], 3), round(matrice_1[i][1], 3), round(matrice_1[i][2], 3)))
    print("\nDecrypted message:")
    print(string)
    return 0


def main():
    error()
    switch()

if __name__ == "__main__":
    main()